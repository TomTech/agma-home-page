﻿using System;
using AgmaHome.Views;
using EO.WebBrowser.Wpf;
using Sapphire.Apps;
using Sapphire.Helpers;
using Sapphire.Data;

namespace AgmaHome.Models
{
    public class MainWindowVM : SapphireWindow
    {
        private MainView _view;
        static DataToken __AgmaUrl = DataToken.Public<string>("setting/AgmaSettings/homeUrl");
        IAppActionItem _home;


        public MainWindowVM()
        {
            _view = new MainView(this);
        }

        protected override void OnInitialize()
        {
            Title = "American Gear Manufacturers Association";

            string url;
            if (Settings.Get(__AgmaUrl, out url))
                AddActionItem("Home", "Home", null, null, url, OnActionItemClicked, ClickMode.RadioButton);
            else
            {
                if (!ResourceHelper.GetWebLinks<MainWindowVM>((title, link) =>
                    AddActionItem(title, title, null, null, link, OnActionItemClicked, ClickMode.RadioButton)))
                {
                    AddActionItem("Home", "Home", null, null, "https://www.agma.org/", OnActionItemClicked, ClickMode.RadioButton);
                }
            }
            _home = Actions[0];
            _home.IsPressed = true; 

            Settings.AddOnDataChanged(__AgmaUrl, OnAgmaUrlChanged);

            _view._WebBrowser_Content.WebView = new WebView();
            _view._WebBrowser_Content.WebView.LoadUrl(url);

            View.Content = _view;
        }


        private void OnAgmaUrlChanged(ISettings service, DataToken token, DataChangeReason reason)
        {
            string url;
            service.Get(token, out url);
            _home.Tag = url;
            _view._WebBrowser_Content.WebView.LoadUrl(url);
        }

        private void OnActionItemClicked(IAppActionItem item)
        {
            if (item.IsPressed)
            {
                try
                {
                    _view._WebBrowser_Content.WebView.LoadUrl((string)item.Tag);
                }
                catch (Exception)
                { }
            }
        }

    }
}