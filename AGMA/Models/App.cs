﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sapphire.Apps;

namespace AgmaHome.Models
{
	public class App : SapphireAppWindows
	{
		protected override void OnInitialize()
		{
			CreateMainWindowAndOpen<MainWindowVM>();
		}
	}
}
